<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`g;/#CN;$t$-dS=b83@G$rMc14dKt3^(uv`MC`r+jM+bF=BD=u|l0N}4_Vy<L*,r');
define('SECURE_AUTH_KEY',  'nwE+r|S({R$.!D4c:|- %1T#}TMYDIO<H>c&=on !2X,$cl8b0>=_ g~%8@p5.Bx');
define('LOGGED_IN_KEY',    'f.xwdSKjr-Ca.k|1!]Q&~N1rbYzG(2jFV1pl53Svm~QVv./3l0X[IeI/Igq6I_oT');
define('NONCE_KEY',        'c3qaeNHa_Cf,}}m%CbJ!Us=)dMXIYlUe8vzmCM<1cknW9~-+9;<5CBRawf/P+IDd');
define('AUTH_SALT',        'VA}J]JE`#KMla0~|RHJ^l::lHaC4YjS4TRBO$;hu-jp|{;I4O#a[J25F6+8Zw{>Z');
define('SECURE_AUTH_SALT', 'BqN H_-dg!evX?M-s@[||jt9 2F;]gh`;#p3jGz1-HvTWW5B,9ggkc5,4vOFt4QI');
define('LOGGED_IN_SALT',   'P|tsZ/JF|m2y?-!0.<x{W_Q}eoyk3xZo w.UbuAf${cC|G9J*r*2cv{Q#|x=l`tc');
define('NONCE_SALT',       'FsN2I5Oh7PxSE|#zLk0GuGj$}K2a!yT/v>K{~9?Kva+xT,giauZpJNmsz_/`0;(k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
